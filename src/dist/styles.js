import lynxColors from '../lynx/dist/lynxColors';

const defStyles = {
  primaryBackgroundColor: '#1A1E34',
  secondaryBackgroundColor: '#525F7F',
  primaryFontColor: '#FFFFFF',
  secondaryFontColor: '#525F7F',
  primaryLabelColor: '#00F2C3',
  fromGradientColor: '#3259F4',
  toGradientColor: '#1D8CF8',
  errorColor: 'red',
  errorBackgroundColor: '#F6B7AC',
  primaryBackground: lynxColors.dark1,
  secondaryBackground: lynxColors.dark2,
  appBackground: lynxColors.dark3,
  primaryColor: lynxColors.green,
};


export default defStyles;
