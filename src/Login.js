import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

import profile from './lynx/image/svg/user.svg';
import logo from './dist/image/logo.png';

import Button from './lynx/components/Button';
import Input from './lynx/components/Input';
import ErrorMessage from './lynx/components/ErrorMessage';

import Helper from './dist/Helper';
import defStyles from './dist/styles';
import Auth from './services/Auth';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      error: '',
    };
  }

  handleChange(prop, data) {
    this.setState({ [prop]: data });
  }

  showError() {
    if (this.state.error) {
      return <ErrorMessage text={this.state.error} />;
    }
    return null;
  }

  executeLogin() {
    Auth.saveUser({ username: this.state.username })
      .then(() => {
        this.props.navigation.navigate('home');
      })
      .catch(error => this.setState({ error: error.message }));
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Image
            source={logo}
            style={{ width: '25%', height: '75%' }}
          />
        </View>
        <View style={styles.contentContainer}>
          <View style={styles.inputContainer}>
            <Input
              image={profile}
              placeholder="Nombre"
              value={this.state.username}
              onChange={data => this.handleChange('username', data)}
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              text="Iniciar"
              colorInit={defStyles.primaryColor}
              colorEnd={defStyles.primaryColor}
              onPress={() => this.executeLogin()}
            />
          </View>
          <View style={styles.errorContainer}>
            {this.showError()}
          </View>
        </View>
        <View style={styles.footerContainer}>
          <Text style={{ color: '#FFFFFF', fontSize: 8 }}>
            Lynx Software Design
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: defStyles.primaryBackground,
    justifyContent: 'center',
  },

  headerContainer: {
    flex: 2,
    alignItems: 'center',
    backgroundColor: defStyles.primaryBackground,
    justifyContent: 'center',
    width: '100%',
  },

  footerContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: defStyles.primaryBackground,
    justifyContent: 'center',
    width: '100%',
  },

  contentContainer: {
    flex: 9,
    alignItems: 'center',
    backgroundColor: defStyles.appBackground,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: defStyles.primaryColor,
    width: '100%',
  },

  errorContainer: {
    alignItems: 'center',
    width: Helper.widthPercentageToDP(85),
    justifyContent: 'center',
  },

  iconContainer: {
    width: '95%',
    alignItems: 'center',
    justifyContent: 'center',
    height: Helper.heightPercentageToDP(40),
  },

  inputContainer: {
    width: Helper.widthPercentageToDP(70),
    alignSelf: 'center',
    height: 30,
    justifyContent: 'center',
  },

  buttonContainer: {
    width: Helper.widthPercentageToDP(45),
    alignSelf: 'center',
    height: 30,
    justifyContent: 'center',
    marginTop: 30,
  },

  error: {
    color: defStyles.errorColor,
  },

});

export default Login;
