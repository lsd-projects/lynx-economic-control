import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';

import Button from './lynx/components/Button';
import InfoCard from './lynx/components/InfoCard';

import Helper from './dist/Helper';
import defStyles from './dist/styles';
import Auth from './services/Auth';
import realmUtil from './services/realmUtil';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { user: { username: '' }, cats: [] };
  }

  componentDidMount() {
    Auth.getUser()
      .then(user => this.setState({ user }))
      .catch(() => this.props.navigation.navigate('login'));

    realmUtil.getCategory().then(a => this.setState({ cats: a }));
  }

  executeLogout() {
    Auth.logout()
      .then(() => this.props.navigation.navigate('login'));
  }

  render() {
    return (
      <ScrollView style={styles.safeAreaContainer}>
        <View style={styles.container}>
          <Text> Bienvenido {this.state.user.username}</Text>
          <Button
            text="Login"
            colorInit={defStyles.primaryColor}
            colorEnd={defStyles.primaryColor}
            onPress={() => this.executeLogout()}
          />
          <View>
            {this.state.cats.map(
              category => (
                <InfoCard
                  title={category.name}
                  color={category.color}
                  info="460"
                />
              ),
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems: 'center',
  },

  errorContainer: {
    flex: 1,
    alignItems: 'center',
    width: '85%',
  },

  iconContainer: {
    width: '95%',
    alignItems: 'center',
    justifyContent: 'center',
    height: Helper.heightPercentageToDP(40),
  },

  inputContainer: {
    width: '95%',
    alignSelf: 'center',
    height: Helper.heightPercentageToDP(15),
  },

  buttonContainer: {
    width: '45%',
    alignSelf: 'center',
    height: Helper.heightPercentageToDP(15),
  },

  error: {
    color: defStyles.errorColor,
  },

  safeAreaContainer: {
    flex: 1,
    backgroundColor: defStyles.appBackground,
  },

});

const TabNavigator = createMaterialTopTabNavigator({
  Home: { screen: Home },
  Settings: { screen: Home },
},
{ tabBarOptions: {
  activeTintColor: defStyles.primaryColor,
  inactiveTintColor: 'gray',
  showIcon: true,
  style: {
    backgroundColor: defStyles.primaryBackground,
  },
  indicatorStyle: {
    backgroundColor: defStyles.primaryColor,
  },
  labelStyle: {
    fontSize: 20,
  },
},
});


export default createAppContainer(TabNavigator);
