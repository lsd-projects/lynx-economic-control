import Realm from 'realm';

// Se crea esquema de la DB

const RealmSchema = {
  User: {
    name: 'User',
    primaryKey: 'idREALM',
    properties: {
      idREALM: 'string',
      username: 'string',
      interactions: { type: 'Interaction[]', default: [] },
    },
  },
  Interaction: {
    name: 'Interaction',
    primaryKey: 'id',
    properties: {
      id: 'string',
      amount: 'double',
      description: 'string',
      type: 'string',
      fecha: 'date',
      category: 'Category',
    },
  },
  Category: {
    name: 'Category',
    primaryKey: 'id',
    properties: {
      id: 'string',
      description: 'string',
      name: 'string',
      color: 'string',
    },
  },
};

// Se construllen relacion de la DB

const RealmConfig = {
  schema: [RealmSchema.User, RealmSchema.Interaction, RealmSchema.Category],
  schemaVersion: 11,
  deleteRealmIfMigrationNeeded: true,
  asdmigration: () => {},
};

// Se agregan datos Default

Realm.open(RealmConfig)
  .then((realm) => {
    realm.write(() => {
      realm.create(RealmSchema.Category.name, {
        id: '1',
        description: 'Dinero destinado a Alimentos o salidas gastronomicas',
        name: 'Alimentos',
        color: '#64DD17',
      });

      realm.create(RealmSchema.Category.name, {
        id: '2',
        description: 'Dinero destinado a tansporte y movilidad',
        name: 'Viaticos',
        color: '#2962FF',
      });

      realm.create(RealmSchema.Category.name, {
        id: '3',
        description: 'Dinero destinado a Combustibles',
        name: 'Combustibles',
        color: '#795548',
      });

      realm.create(RealmSchema.Category.name, {
        id: '4',
        description: 'Dinero destinado a voletas o facturas varias',
        name: 'Voletas',
        color: '#F50057',
      });

      realm.create(RealmSchema.Category.name, {
        id: '5',
        description: 'Dinero destinado a salidas cultirales o entretenimiento',
        name: 'Entretenimiento',
        color: '#FFC107',
      });
    });
  });

// Se exportan mensajes publicos

export default {
  isLoggedIn: () => (
    Realm.open(RealmConfig)
      .then(realm => realm.objectForPrimaryKey(RealmSchema.User.name, 'realmUser'))
  ),
  saveUser: user => (
    Realm.open(RealmConfig)
      .then(realm => realm.write(() => realm.create(RealmSchema.User.name, { ...user, idREALM: 'realmUser' })))
      .then(() => user)
  ),
  getUser: () => (
    Realm.open(RealmConfig)
      .then(realm => realm.objectForPrimaryKey(RealmSchema.User.name, 'realmUser'))
  ),
  getCategory: () => (
    Realm.open(RealmConfig)
      .then(realm => realm.objects(RealmSchema.Category.name))
  ),
  logout: () => (
    Realm.open(RealmConfig)
      .then((realm) => {
        const user = realm.objectForPrimaryKey(RealmSchema.User.name, 'realmUser');
        realm.write(() => realm.delete(user));
      })
  ),
};
