import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import defStyles from '../../dist/styles';
import Helper from '../../dist/Helper';

export default class Input extends Component {
  componentDidMount() {

  }

  render() {
    return (
      <View style={{
        width: Helper.widthPercentageToDP(80),
        height: Helper.heightPercentageToDP(20),
        backgroundColor: this.props.color,
        marginTop: 30,
      }}
      >
        <TouchableOpacity style={styles.flex1} onPress={() => this.props.onPress()}>
          <Text> {this.props.title} </Text>
          <Text> {this.props.info} {this.props.color} </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  flex1: {
    flex: 1,
    width: '100%',
  },

  center: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Helper.heightPercentageToDP(2.5),
  },

  text: {
    color: defStyles.primaryFontColor,
  },

});
