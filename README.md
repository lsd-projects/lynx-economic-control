# Lynx Economic Control
---
Es una aplicacion desarrollada en react native que nos permite llevar un control de los gastos que realizamos para poder saber en que estamos gastando nuestro dinero y corregir los habitos economicos

## Instalacion
---
Para instalarlo debemos ejecutar los siguientes codigos (Recuerde que el tutorial esta realizado en Linux y es fiel para sistemas MacOS pero en caso de utilizar DOS puede no ser similar a no ser que use GitShell)

### Andoird
```
git clone https://gitlab.com/lsd-projects/lynx-economic-control.git
cd lynx-economic-control
npm install
react-native run-andoid
```

### iOS
```
git clone https://gitlab.com/lsd-projects/lynx-economic-control.git
cd lynx-economic-control
npm install
react-native run-ios
```


## Fix en Linux
---

En caso de que en linux dispongamos de un error al intentar ejecutarlo debemos ejecutar el siguiente comando:

``` 
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p 
```
